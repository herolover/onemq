#include "buffer.h"

#include <cstring>

omq::Buffer::Buffer(unsigned capacity)
  : capacity_(capacity)
  , left_bound_(0)
  , right_bound_(0)
{
  data_ = new char [capacity_];
}

omq::Buffer::~Buffer()
{
  delete[] data_;
}

const char * omq::Buffer::data() const
{
  return data_ + left_bound_;
}

unsigned omq::Buffer::size() const
{
  return right_bound_ - left_bound_;
}

bool omq::Buffer::empty() const
{
  return left_bound_ == right_bound_;
}

int omq::Buffer::find(const std::string &str) const
{
  int pos = -1;
  for (unsigned i = left_bound_; i < right_bound_; ++i)
  {
    bool found = true;
    for (unsigned j = 0; j < str.size(); ++j)
    {
      if (data_[i + j] != str[j])
      {
        found = false;
        break;
      }
    }

    if (found)
    {
      pos = i;
      break;
    }
  }

  return pos;
}

bool omq::Buffer::read(char *data, unsigned size)
{
  if (this->size() >= size)
  {
    std::memcpy(data, data_ + left_bound_, size);
    left_bound_ += size;

    this->reset_if_possible();

    return true;
  }

  return false;
}

bool omq::Buffer::read(std::string &data, unsigned size)
{
  if (this->size() >= size)
  {
    data.assign(data_ + left_bound_, size);
    left_bound_ += size;

    this->reset_if_possible();

    return true;
  }

  return false;
}

bool omq::Buffer::read_until(std::string &data, const std::string &delimiter)
{
  int pos = this->find(delimiter);
  if (pos != -1)
  {
    data.assign(data_ + left_bound_, pos - left_bound_);
    left_bound_ = pos + delimiter.size();

    this->reset_if_possible();

    return true;
  }

  return false;
}

bool omq::Buffer::ignore(unsigned size)
{
  if (this->size() >= size)
  {
    left_bound_ += size;

    this->reset_if_possible();

    return true;
  }

  return false;
}

void omq::Buffer::write(const char *data, unsigned size)
{
  if (right_bound_ + size <= capacity_)
  {
    std::memcpy(data_ + right_bound_, data, size);
    right_bound_ += size;
  }
  else
  {
    /*
     * Expand the buffer and copy data.
     */
    unsigned new_capacity = (right_bound_ + size) * 2;
    char *old_data = data_;
    char *new_data = new char [new_capacity];
    std::memcpy(new_data, old_data, capacity_);
    std::memcpy(new_data + right_bound_, data, size);
    capacity_ = new_capacity;
    data_ = new_data;
    right_bound_ += size;
    delete[] old_data;
  }
}

void omq::Buffer::write(const std::string &data)
{
  this->write(data.c_str(), data.size());
}

/*
 * If left_bound_ is equal to right_bound then buffer is empty and we can set
 * both bounds to zero.
 */
void omq::Buffer::reset_if_possible()
{
  if (left_bound_ == right_bound_)
  {
    left_bound_ = 0;
    right_bound_ = 0;
  }
}
