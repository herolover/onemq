#ifndef NOTIFIER_H
#define NOTIFIER_H

#include <atomic>
#include <thread>

namespace omq
{
  class Notifier
  {
  public:
    Notifier()
      : is_notified_(false)
    {
    }

    void notify()
    {
      is_notified_.store(true);
    }

    void wait()
    {
      while (!is_notified_)
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
      }
      is_notified_.store(false);
    }
  private:
    std::atomic_bool is_notified_;
  };
}

#endif // NOTIFIER_H
