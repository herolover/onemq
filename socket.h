#ifndef SOCKET_H
#define SOCKET_H

#include <unordered_set>

#include "io_manager.h"
#include "thread_safe_queue.h"

namespace omq
{
  class Socket
  {
  public:
    Socket(IOManager &io_manager);

    void bind(const std::string &endpoint);
    void connect(const std::string &endpoint);

    const std::unordered_set<SocketDescriptor> & connections();

    bool read(MessagePtr &message, bool dontwait);
    void write(SocketDescriptor socket, const char *data, unsigned size);

  private:
    IOManager &io_manager_;
    ThreadSafeQueue<MessagePtr> messages_;
    std::unordered_set<SocketDescriptor> connections_;

    std::unordered_set<std::string> connecting_endpoints_;
    std::unordered_set<std::string> listening_endpoints_;
  };
}

#endif // SOCKET_H
