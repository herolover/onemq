#include "epoller.h"

#include <iostream>
#include <cstring>
#include <unistd.h>

unsigned omq_events_to_epoll_events(int events)
{
  unsigned epoll_events = 0;

  if ((events & omq::E_IN) != 0)
  {
    epoll_events |= EPOLLIN;
  }
  if ((events & omq::E_OUT) != 0)
  {
    epoll_events |= EPOLLOUT;
  }
  if ((events & omq::E_ERR) != 0)
  {
    epoll_events |= EPOLLERR;
  }

  return epoll_events;
}

omq::Epoller::Epoller()
  : BasePoller()
{
  fd_ = epoll_create(1);
  if (fd_ == -1)
  {
    std::cerr << strerror(get_last_error()) << std::endl;
  }
}

omq::Epoller::~Epoller()
{
  if (close(fd_) == -1)
  {
    std::cerr << strerror(get_last_error()) << std::endl;
  }
}

void omq::Epoller::add_socket(SocketDescriptor socket, int events)
{
  int operation = 0;
  if (sockets_.count(socket) == 0)
  {
    operation = EPOLL_CTL_ADD;

    sockets_[socket] = events;
  }
  else
  {
    operation = EPOLL_CTL_MOD;

    sockets_[socket] |= events;
    events = sockets_[socket];
  }

  epoll_event event;
  event.data.fd = socket;
  event.events = omq_events_to_epoll_events(events);
  if (epoll_ctl(fd_, operation, socket, &event) == -1)
  {
    std::cerr << strerror(get_last_error()) << std::endl;
  }
}

void omq::Epoller::remove_socket(omq::SocketDescriptor socket, int events)
{
  if (sockets_.count(socket) == 0)
  {
    return;
  }

  sockets_[socket] &= ~events;

  int operation = 0;
  if (sockets_[socket] == E_NO)
  {
    operation = EPOLL_CTL_DEL;

    sockets_.erase(socket);
  }
  else
  {
    operation = EPOLL_CTL_MOD;

    events = sockets_[socket];
  }

  epoll_event event;
  event.data.fd = socket;
  event.events = omq_events_to_epoll_events(events);
  if (epoll_ctl(fd_, operation, socket, &event) == -1)
  {
    std::cerr << strerror(get_last_error()) << std::endl;
  }
}

const std::unordered_map<omq::SocketDescriptor, int> & omq::Epoller::poll(int timeout)
{
  socket_events_.clear();

  int sockets_number = epoll_wait(fd_, events_, MAX_EVENTS_, timeout);
  if (sockets_number == -1)
  {
    std::cerr << strerror(get_last_error()) << std::endl;
  }
  else
  {
    for (int i = 0; i < sockets_number; ++i)
    {
      unsigned events = events_[i].events;
      SocketDescriptor socket = events_[i].data.fd;

      if (events & EPOLLIN)
      {
        socket_events_[socket] |= E_IN;
      }
      if (events & EPOLLOUT)
      {
        socket_events_[socket] |= E_OUT;
      }
      if (events & EPOLLERR)
      {
        socket_events_[socket] |= E_ERR;
      }
    }
  }

  return socket_events_;
}
