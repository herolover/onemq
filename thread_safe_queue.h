#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H

#include <queue>

#include "notifier.h"
#include "spinlock.h"

namespace omq
{
  template<class T>
  class ThreadSafeQueue
  {
  public:

    void push(const T &value)
    {
      {
        std::lock_guard<SpinLock> guard(lock_);
        queue_.push(value);
      }
      notifier_.notify();
    }

    void pop(T &value)
    {
      std::lock_guard<SpinLock> guard(lock_);
      value = queue_.front();
      queue_.pop();
    }

    bool empty()
    {
      std::lock_guard<SpinLock> guard(lock_);

      return queue_.empty();
    }

    void wait()
    {
      notifier_.wait();
    }

  private:
    SpinLock lock_;
    std::queue<T> queue_;
    Notifier notifier_;
  };
}

#endif // THREAD_SAFE_QUEUE_H
