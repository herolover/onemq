#ifndef IO_MANAGER_H
#define IO_MANAGER_H

#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include <thread>
#include <atomic>
#include <functional>
#include <vector>

#include "common.h"
#include "poller.h"
#include "buffer.h"
#include "thread_safe_queue.h"

namespace omq
{
  struct Message
  {
    inline
    Message()
      : data(1024)
      , from(INVALID_SOCKET)
    {
    }

    Buffer data;
    SocketDescriptor from;
  };

  typedef std::shared_ptr<Message> MessagePtr;

  class IOManager
  {
  public:
    typedef std::function<void (Buffer &, Buffer &)> ReaderType;
    typedef std::function<void (Buffer &, const char *, unsigned)> WriterType;

    IOManager(const ReaderType &reader, const WriterType &writer);
    ~IOManager();

    void add_for_connecting(const std::string &endpoint,
                            ThreadSafeQueue<MessagePtr> *messages);
    void add_for_listening(const std::string &endpoint,
                           ThreadSafeQueue<MessagePtr> *messages);
    void sync_outgoing_connections(const std::string &endpoint,
                                   std::unordered_set<SocketDescriptor> &sockets);
    void sync_incoming_connections(const std::string &endpoint,
                                   std::unordered_set<SocketDescriptor> &sockets);

    void write(SocketDescriptor socket, const std::string &data);
    void write(SocketDescriptor socket, const char *data, unsigned size);

    void start(int timeout=10);
    void stop();
  private:
    void run(int timeout);
    void send(SocketDescriptor socket);
    void receive(SocketDescriptor socket);
    void accept(SocketDescriptor socket);
    void connect(SocketDescriptor socket);

    ReaderType reader_;
    WriterType writer_;

    /*
     * Tasks are executed is the async thread before events polling.
     */
    ThreadSafeQueue<std::function<void ()>> tasks_;

    std::unordered_map<SocketDescriptor, std::pair<std::string, ThreadSafeQueue<MessagePtr> *>> connecting_endpoints_;
    std::unordered_map<SocketDescriptor, std::pair<std::string, ThreadSafeQueue<MessagePtr> *>> listening_endpoints_;

    std::unordered_map<std::string, SocketDescriptor> new_outgoing_connections_;
    std::unordered_map<std::string, std::unordered_set<SocketDescriptor>> new_incoming_connections_;
    std::unordered_set<SocketDescriptor> new_terminated_connections_;

    struct IOBuffer
    {
      IOBuffer()
        : input_buffer(100 * 1024)
        , output_buffer(100 * 1024)
      {
      }

      Buffer input_buffer;
      Buffer output_buffer;
    };

    std::unordered_map<SocketDescriptor, std::pair<std::unique_ptr<IOBuffer>, ThreadSafeQueue<MessagePtr> *>> connections_;
    Poller poller_;

    std::thread thread_;
    std::atomic_bool is_started_;
  };
}

#endif // IO_MANAGER_H
