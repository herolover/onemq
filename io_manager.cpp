#include "io_manager.h"

#include <functional>
#include <iostream>
#include <chrono>
#include <condition_variable>
#include <algorithm>

#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

sockaddr_in make_sockaddr_in(const std::string &endpoint)
{
  unsigned pos = endpoint.find(':');
  std::string address = endpoint.substr(0, pos);
  int port = std::stoi(endpoint.substr(pos + 1));

  sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(port);

  if (address != "*")
  {
    inet_pton(AF_INET, address.c_str(), &addr.sin_addr);
  }
  else
  {
    addr.sin_addr.s_addr = INADDR_ANY;
  }

  return addr;
}

int set_blocking(omq::SocketDescriptor socket, bool enable)
{
  int flags = fcntl(socket, F_GETFL);
  if (flags == -1)
  {
    return -1;
  }

  if (enable)
  {
    flags &= ~O_NONBLOCK;
  }
  else
  {
    flags |= O_NONBLOCK;
  }

  return fcntl(socket, F_SETFL, flags);
}

omq::SocketDescriptor connect(const std::string &endpoint)
{
  omq::SocketDescriptor socket = ::socket(AF_INET, SOCK_STREAM, 0);
  if (socket == omq::INVALID_SOCKET)
  {
    std::cerr << strerror(omq::get_last_error()) << std::endl;
    return omq::INVALID_SOCKET;
  }
  if (set_blocking(socket, false) == -1)
  {
    std::cerr << strerror(omq::get_last_error()) << std::endl;
    return omq::INVALID_SOCKET;
  }

  sockaddr_in addr = make_sockaddr_in(endpoint);
  int rc = connect(socket, (const sockaddr *)&addr, sizeof(addr));
  if (rc == -1 && omq::get_last_error() != EAGAIN)
  {
    std::cerr << strerror(omq::get_last_error()) << std::endl;
    return omq::INVALID_SOCKET;
  }

  return socket;
}

omq::IOManager::IOManager(const omq::IOManager::ReaderType &reader,
                          const omq::IOManager::WriterType &writer)
  : reader_(reader)
  , writer_(writer)
{
  start(10);
}

omq::IOManager::~IOManager()
{
  if (is_started_)
  {
    stop();
  }
}

void omq::IOManager::add_for_connecting(const std::string &endpoint,
                                        ThreadSafeQueue<MessagePtr> *messages)
{
  tasks_.push([this, endpoint, messages]()
  {
    SocketDescriptor socket = ::connect(endpoint);
    if (socket == INVALID_SOCKET)
    {
      std::cerr << strerror(get_last_error()) << std::endl;
      return;
    }

    connecting_endpoints_[socket] = std::make_pair(endpoint, messages);

    poller_.add_socket(socket, E_OUT | E_ERR);
  });
}

void omq::IOManager::add_for_listening(const std::string &endpoint,
                                       ThreadSafeQueue<MessagePtr> *messages)
{
  tasks_.push([this, endpoint, messages]()
  {
    SocketDescriptor socket = ::socket(AF_INET, SOCK_STREAM, 0);
    if (socket == INVALID_SOCKET)
    {
      std::cerr << strerror(get_last_error()) << std::endl;
      return;
    }
    if (set_blocking(socket, false) == -1)
    {
      std::cerr << strerror(get_last_error()) << std::endl;
      return;
    }

    int value = 1;
    if (setsockopt(socket, SOL_SOCKET, SO_REUSEADDR, &value, sizeof(value)) == -1)
    {
      std::cerr << strerror(get_last_error()) << std::endl;
      return;
    }

    sockaddr_in addr = make_sockaddr_in(endpoint);
    if (bind(socket, (const sockaddr *)&addr, sizeof(addr)) == -1)
    {
      std::cerr << strerror(get_last_error()) << std::endl;
      return;
    }

    if (listen(socket, 8) == -1)
    {
      std::cerr << strerror(get_last_error()) << std::endl;
      return;
    }

    listening_endpoints_[socket] = std::make_pair(endpoint, messages);

    poller_.add_socket(socket, E_IN | E_ERR);
  });
}

void omq::IOManager::sync_incoming_connections(const std::string &endpoint,
                                              std::unordered_set<SocketDescriptor> &sockets)
{
  Notifier notifier;

  tasks_.push([this, &notifier, endpoint, &sockets]()
  {
    auto terminated_socket = new_terminated_connections_.begin();
    while (terminated_socket != new_terminated_connections_.end())
    {
      if (sockets.erase(*terminated_socket) > 0)
      {
        terminated_socket = new_terminated_connections_.erase(terminated_socket);
      }
      else
      {
        ++terminated_socket;
      }
    }

    for (auto &socket: new_incoming_connections_[endpoint])
    {
      sockets.insert(socket);
    }
    new_incoming_connections_[endpoint].clear();

    notifier.notify();
  });

  notifier.wait();
}

void omq::IOManager::write(SocketDescriptor socket, const std::string &data)
{
  this->write(socket, data.c_str(), data.size());
}

void omq::IOManager::write(omq::SocketDescriptor socket, const char *data, unsigned size)
{
  Notifier notifier;

  tasks_.push([this, &notifier, socket, data, size]()
  {
    writer_(connections_[socket].first->output_buffer, data, size);

    notifier.notify();

    poller_.add_socket(socket, E_OUT | E_ERR);
  });

  notifier.wait();
}

void omq::IOManager::start(int timeout)
{
  is_started_.store(true);
  thread_ = std::thread(std::bind(&IOManager::run, this, timeout));
}

void omq::IOManager::stop()
{
  is_started_.store(false);
  thread_.join();
}

void omq::IOManager::run(int timeout)
{
  while (is_started_)
  {
    {
      while (!tasks_.empty())
      {
        std::function<void ()> task;
        tasks_.pop(task);
        task();
      }
    }

    auto events = poller_.poll(timeout);
    if (!events.empty())
    {
      bool errors_only = true;

      for (auto &event: events)
      {
        if ((event.second & E_ERR) == 0)
        {
          errors_only = false;

          if (event.second & E_IN)
          {
            if (connections_.count(event.first) == 1)
            {
              this->receive(event.first);
            }
            else if (listening_endpoints_.count(event.first) == 1)
            {
              this->accept(event.first);
            }
            else
            {
              std::cerr << "That is very interesting." << std::endl;
            }
          }

          if (event.second & E_OUT)
          {
            if (connections_.count(event.first) == 1)
            {
              this->send(event.first);
            }
            else if (connecting_endpoints_.count(event.first) == 1)
            {
              this->connect(event.first);
            }
            else
            {
              std::cerr << "That is very interesting." << std::endl;
            }
          }
        }
        else
        {
          int error;
          socklen_t error_len = sizeof(error);
          if (getsockopt(event.first, SOL_SOCKET, SO_ERROR, &error, &error_len) == -1)
          {
            std::cerr << strerror(get_last_error()) << std::endl;
          }
          else
          {
            std::cerr << strerror(error) << std::endl;
          }
        }
      }

      if (errors_only)
      {
        std::cout << "Errors only" << std::endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
      }
    }
  }
}

void omq::IOManager::send(omq::SocketDescriptor socket)
{
  Buffer &output_buffer = connections_[socket].first->output_buffer;

  while (output_buffer.size() > 0)
  {
    int bytes_sent = ::send(socket, output_buffer.data(), output_buffer.size(), 0);
    if (bytes_sent > 0)
    {
      output_buffer.ignore(bytes_sent);
    }
    else if (bytes_sent == -1)
    {
      if (get_last_error() != EAGAIN)
      {
        std::cerr << strerror(get_last_error()) << std::endl;
      }
      return;
    }
  }

  poller_.remove_socket(socket, E_OUT);
}

void omq::IOManager::receive(omq::SocketDescriptor socket)
{
  while (true)
  {
    char temp_buffer[1024];
    int bytes_received = recv(socket, temp_buffer, 1024, 0);
    if (bytes_received > 0)
    {
      connections_[socket].first->input_buffer.write(temp_buffer, bytes_received);
    }
    else
    {
      if (bytes_received == -1)
      {
        if (get_last_error() == EAGAIN)
        {
          MessagePtr message = std::make_shared<Message>();
          reader_(connections_[socket].first->input_buffer, message->data);
          if (!message->data.empty())
          {
            message->from = socket;
            connections_[socket].second->push(message);
          }
        }
        else
        {
          std::cerr << strerror(get_last_error()) << std::endl;
        }
      }
      else if (bytes_received == 0)
      {
        connections_.erase(socket);
        new_terminated_connections_.insert(socket);
        poller_.remove_socket(socket, E_ALL);
        if (close(socket) == -1)
        {
          std::cerr << strerror(get_last_error()) << std::endl;
        }
      }
      return;
    }
  }
}

void omq::IOManager::accept(omq::SocketDescriptor socket)
{
  SocketDescriptor new_socket = ::accept(socket, nullptr, nullptr);
  if (new_socket != INVALID_SOCKET)
  {
    if (set_blocking(new_socket, false) == -1)
    {
      std::cerr << strerror(get_last_error()) << std::endl;
    }
    else
    {
      new_incoming_connections_[listening_endpoints_[socket].first].insert(new_socket);
      connections_[new_socket] = std::make_pair(std::unique_ptr<IOBuffer>(new IOBuffer()),
                                                listening_endpoints_[socket].second);

      poller_.add_socket(new_socket, E_IN | E_ERR);
    }
  }
  else
  {
    std::cerr << strerror(get_last_error()) << std::endl;
  }
}

void omq::IOManager::connect(omq::SocketDescriptor socket)
{
}
