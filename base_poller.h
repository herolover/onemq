#ifndef BASE_POLLER_H
#define BASE_POLLER_H

#include <unordered_map>

#include "common.h"

namespace omq
{
  enum PollEvent
  {
    E_NO = 0,
    E_IN = 1,
    E_OUT = 2,
    E_ERR = 4,
    E_ALL = 7
  };

  class BasePoller
  {
  public:
    virtual ~BasePoller()
    {
    }

    virtual void add_socket(SocketDescriptor socket, int events) = 0;
    virtual void remove_socket(SocketDescriptor socket, int events) = 0;
    virtual const std::unordered_map<SocketDescriptor, int> & poll(int timeout) = 0;

  protected:
    std::unordered_map<SocketDescriptor, int> socket_events_;
  };
}

#endif // BASE_POLLER_H
