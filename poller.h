#ifndef POLLER_H
#define POLLER_H

#include "epoller.h"

namespace omq
{
#ifdef __gnu_linux__
  typedef Epoller Poller;
#endif
}

#endif // POLLER_H
