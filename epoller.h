#ifndef EPOLLER_H
#define EPOLLER_H

#include <unordered_map>

#include <sys/epoll.h>

#include "base_poller.h"

namespace omq
{
  class Epoller: public BasePoller
  {
  public:
    Epoller();
    ~Epoller();

    void add_socket(SocketDescriptor socket, int events) final;
    void remove_socket(SocketDescriptor socket, int events) final;
    const std::unordered_map<SocketDescriptor, int> & poll(int timeout) final;

  private:
    static const int MAX_EVENTS_ = 32;
    epoll_event events_[MAX_EVENTS_];
    int fd_;

    std::unordered_map<SocketDescriptor, int> sockets_;
  };
}

#endif // EPOLLER_H
