#include <iostream>

#include "../../io_manager.h"
#include "../../socket.h"

void reader(omq::Buffer &input_buffer, omq::Buffer &message_data)
{
  int pos = input_buffer.find("\n");
  if (pos != -1)
  {
    message_data.write(input_buffer.data(), pos);
    input_buffer.ignore(pos + 1);
  }
}

void writer(omq::Buffer &output_buffer, const char *data, unsigned size)
{
  output_buffer.write(data, size);
  output_buffer.write("\n");
}

int main()
{
  omq::IOManager io_manager(reader, writer);

  omq::Socket socket(io_manager);
  socket.bind("*:1234");

  while (!std::cin.eof())
  {
    std::string message;
    std::getline(std::cin, message);

    auto &connections = socket.connections();
    std::cout << connections.size() << std::endl;

    for (auto &connection: connections)
    {
      socket.write(connection, message.c_str(), message.size());
    }
  }

  return 0;
}
