import qbs 1.0

CppApplication {
    name: "pub_server"

    Depends {
        name: "onemq"
    }

    files: [
        "server.cpp"
    ]

    cpp.cxxFlags: [
        "-std=c++11"
    ]

    cpp.linkerFlags: [
        "-pthread"
    ]
}
