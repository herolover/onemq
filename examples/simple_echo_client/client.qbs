import qbs 1.0

CppApplication {
    name: "simple_echo_client"

    Depends {
        name: "onemq"
    }

    files: [
        "client.cpp"
    ]

    cpp.cxxFlags: [
        "-std=c++11"
    ]
}
