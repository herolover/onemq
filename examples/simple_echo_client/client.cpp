#include <iostream>

#include "../../tcp_socket.h"
#include "../../messenger.h"

int main()
{
  omq::TCPSocket socket(false);
  socket.connect("127.0.0.1", 12345);

  omq::DelimiterMessenger messenger(socket, "\n");

  std::cout << "Connected." << std::endl;

  while (!std::cin.eof())
  {
    std::string message;
    std::getline(std::cin, message);

    messenger.write_message(message.c_str(), message.size());
    message = messenger.read_message();
    std::cout << message << std::endl;
  }

  socket.close();

  std::cout << "Disconnected." << std::endl;

  return 0;
}
