import qbs 1.0

CppApplication {
    name: "simple_echo_server"

    Depends {
        name: "onemq"
    }

    files: [
        "server.cpp"
    ]

    cpp.cxxFlags: [
        "-std=c++11"
    ]

    cpp.linkerFlags: [
        "-pthread"
    ]
}
