#include <iostream>

#include "../../io_manager.h"
#include "../../socket.h"

void reader(omq::Buffer &input_buffer, omq::Buffer &message_data)
{
  int pos = input_buffer.find("\n");
  if (pos != -1)
  {
    message_data.write(input_buffer.data(), pos);
    input_buffer.ignore(pos + 1);
  }
}

void writer(omq::Buffer &output_buffer, const char *data, unsigned size)
{
  output_buffer.write(data, size);
  output_buffer.write("\n");
}

int main()
{
  omq::IOManager io_manager(reader, writer);

  omq::Socket socket(io_manager);
  socket.bind("*:1234");

  while (!std::cin.eof())
  {
    omq::MessagePtr message;
    socket.read(message, false);

    std::cout << "Received message from " << message->from << ": ";
    std::cout.write(message->data.data(), message->data.size());
    std::cout << std::endl;

    socket.write(message->from, message->data.data(), message->data.size());
  }

  return 0;
}
