#ifndef COMMON_H
#define COMMON_H

#include <errno.h>

namespace omq
{
  typedef int SocketDescriptor;
#ifdef __gnu_linux__
  const int INVALID_SOCKET = -1;
#endif

  inline
  int get_last_error()
  {
    return errno;
  }
}

#endif // COMMON_H
