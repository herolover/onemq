import qbs 1.0

Project {
    StaticLibrary {
        name: "onemq"

        Depends {
            name: "cpp"
        }

        files: [
            "common.h",
            "base_poller.h",
            "epoller.h",
            "epoller.cpp",
            "poller.h",
            "buffer.h",
            "buffer.cpp",
            "thread_safe_queue.h",
            "notifier.h",
            "spinlock.h",
            "socket.h",
            "socket.cpp",
            "io_manager.h",
            "io_manager.cpp"
        ]

        cpp.cxxFlags: [
            "-std=c++11"
        ]

        cpp.linkerFlags: [
            "-pthread"
        ]
    }

    references: [
        "examples/pub_server/server.qbs",
        "examples/simple_echo_server/server.qbs"
    ]
}
