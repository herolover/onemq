#ifndef BUFFER_H
#define BUFFER_H

#include <atomic>
#include <string>

namespace omq
{
  class Buffer
  {
  public:
    Buffer(unsigned capacity);
    ~Buffer();

    const char *data() const;
    unsigned size() const;
    bool empty() const;
    int find(const std::string &str) const;
    bool read(char *data, unsigned size);
    bool read(std::string &data, unsigned size);
    bool read_until(std::string &data, const std::string &delimiter);
    bool ignore(unsigned size);
    void write(const char *data, unsigned size);
    void write(const std::string &data);

  private:
    void reset_if_possible();

    char * data_;
    unsigned capacity_;
    unsigned left_bound_;
    unsigned right_bound_;
  };
}

#endif // BUFFER_H
