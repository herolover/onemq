#include "socket.h"

omq::Socket::Socket(omq::IOManager &io_manager)
  : io_manager_(io_manager)
{
}

void omq::Socket::bind(const std::string &endpoint)
{
  io_manager_.add_for_listening(endpoint, &messages_);
  listening_endpoints_.insert(endpoint);
}

void omq::Socket::connect(const std::string &endpoint)
{
  io_manager_.add_for_connecting(endpoint, &messages_);
  connecting_endpoints_.insert(endpoint);
}

const std::unordered_set<omq::SocketDescriptor> & omq::Socket::connections()
{
  for (auto &endpoint: listening_endpoints_)
  {
    io_manager_.sync_incoming_connections(endpoint, connections_);
  }

  for (auto &endpoint: connecting_endpoints_)
  {
//    io_manager_.sync_outgoing_connections(endpoint, connections_);
  }

  return connections_;
}

bool omq::Socket::read(MessagePtr &message, bool dontwait)
{
  if (messages_.empty())
  {
    if (dontwait)
    {
      return false;
    }
    else
    {
      messages_.wait();
    }
  }

  messages_.pop(message);

  return true;
}

void omq::Socket::write(omq::SocketDescriptor socket, const char *data, unsigned size)
{
  io_manager_.write(socket, data, size);
}
