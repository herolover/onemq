#ifndef SPINLOCK_H
#define SPINLOCK_H

#include <atomic>
#include <thread>

namespace omq
{
  class SpinLock
  {
  public:
    SpinLock()
      : flag_(ATOMIC_FLAG_INIT)
    {
    }

    void lock()
    {
      while (flag_.test_and_set(std::memory_order_acquire))
      {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
      }
    }

    void unlock()
    {
      flag_.clear(std::memory_order_release);
    }

  private:
    std::atomic_flag flag_;
  };
}

#endif // SPINLOCK_H
